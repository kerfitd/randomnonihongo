class Card {
    constructor(hidden, shown = []) {
        this.hidden = hidden;
        this.shown = shown;
    }

    flip() {
        if (this.hidden.length === 0) {
            return null;
        }
        const nextFace = this.hidden[0];
        return new Card(_.tail(this.hidden), _.concat(this.shown, nextFace));
    }
}
const randomEngine = Random.engines.mt19937().autoSeed();
class Pattern {
    constructor(elements, cardBuilder) {
        this.elements = elements;
        this.cardBuilder = cardBuilder;
    }

    buildCard() {
        return new Card(this.cardBuilder(_.mapValues(
            this.elements,
            element => Random.pick(randomEngine, element)
        )));
    }
}

function toFlip(fn) {
  document.getElementById("tehbtn").onclick = fn;
}

function flip(card) {
  if (card == null) {
    card = patterns[Math.floor(Math.random() * patterns.length)].buildCard().flip();
  }
  document.getElementById("tehdiv").innerHTML = card.shown.join("<br/>");
  card = card.flip();
  toFlip(() => flip(card));
}

function init() {
  toFlip(() => flip(null));

  document.getElementById("tehdiv").innerHTML = "Нажимай кнопку.";
  document.getElementById("tehbtn").disabled = false;
}